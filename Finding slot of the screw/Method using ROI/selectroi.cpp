#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main()
{
   
    Mat img;
   img = imread("intermediate.jpg") ; 
  

   Rect r = selectROI(img); 

   Mat roi = img(r);

    imshow("Image", roi);
    waitKey(0);

    

Mat res = img.clone();

rectangle(res, r, Scalar(0,255,0), 1);


namedWindow("Step 4  Final result", WINDOW_AUTOSIZE);


 imshow("Step 4 Final result", res);



 waitKey(1000);


 imwrite("final_2.jpg", res);



   
    return 0;
}
