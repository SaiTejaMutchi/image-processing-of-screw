#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
    Mat img = imread("out_1.png");

    int th_blob_size = 100;

    Mat gray;
    cvtColor(img, gray, COLOR_BGR2GRAY);

    Mat hsv;
    threshold(gray, hsv, 50, 255, THRESH_BINARY);




    Mat mask;
    inRange(hsv, Scalar(0, 0, 220), Scalar(180, 30, 255), mask);

    vector<vector<Point>> contours;
    findContours(hsv, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);

    Mat res = img.clone();
    for (int i = 0; i < contours.size(); ++i)
    {
        // Remove small blobs
        if (contours[i].size() < 4)
        {
            continue;
        }
        RotatedRect R=minAreaRect(contours[i]);
        if(((contourArea(contours[i]))/(R.size.height * R.size.width))<0.80) continue;

    if(1000<contourArea(contours[i])<15000){ 
        Rect box = boundingRect(contours[i]);
        rectangle(res, box, Scalar(0,255,0), 1);
        
        }

    }
    imwrite("slot_1.jpg", res);

    return 0;
}


