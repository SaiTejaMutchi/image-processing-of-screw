## Assignment

### Task-1 Find the outer circle of screw from images 
- Applying Hough Transform to the given image, found outer circle of screw

input: ![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20outer%20circle%20of%20screw/Input/1.jpg)

Output:![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20outer%20circle%20of%20screw/Output/out_1.png)

### Task-2 Find Shank diameter from image
- Applying ROI  
Input: ![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20Shank%20Diameter/Input/img_1.jpg)
Output: ![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20Shank%20Diameter/Output/final_1.jpg) 
- Similarly this can also be applied to the slot task and we need to rotate it.

### Task-3  Find slot from image
- Applying contours in order find the rectangle

input: ![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20slot%20of%20the%20screw/Input/out_1.png)
Output: ![](https://gitlab.com/SaiTejaMutchi/image-processing-of-screw/-/raw/master/Finding%20slot%20of%20the%20screw/Output/slot_1.jpg)
