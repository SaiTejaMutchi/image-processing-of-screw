#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include <iostream>


using namespace std;
using namespace cv;

int main(){

  Mat gray, img;



// Read the image as gray-scale
img = imread("5.jpg");
// Convert to gray-scale
cvtColor(img, gray, COLOR_BGR2GRAY);

// image height and width,

int height, width;
height=img.rows;
width=img.cols;

// Blur the image to reduce noise 
Mat img_blur;
medianBlur(gray, img_blur, 5);
// Create a vector for detected circles
vector<Vec3f>  circles;
// Apply Hough Transform
HoughCircles(img_blur, circles, HOUGH_GRADIENT, 2, 100);
// Create mask image
Mat mask = img.clone();

mask.setTo(Scalar(0,0,0));



// Draw detected circles

for(size_t i=0; i<circles.size(); i++) {
    Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
    int radius = cvRound(circles[i][2]);
    circle(mask, center, radius, Scalar(255, 255, 255), -1,8,0);
}

Mat circleimg;

bitwise_and(img, mask, circleimg),

imwrite("out_5.png", circleimg);
}
